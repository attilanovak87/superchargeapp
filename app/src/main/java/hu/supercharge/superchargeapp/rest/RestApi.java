package hu.supercharge.superchargeapp.rest;

import hu.supercharge.superchargeapp.model.SearchResponse;
import hu.supercharge.superchargeapp.model.MovieDetailsResponse;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface RestApi {

    @GET("search/movie")
    Observable<SearchResponse> getMovies(@Query("query") String searchTerm, @Query("page") Integer page, @Query("include_adult") Boolean includeAdult);

    @GET("movie/{movie_id}")
    Observable<MovieDetailsResponse> getMovieDetails(@Path(value = "movie_id", encoded = true) Integer movieId);

}
