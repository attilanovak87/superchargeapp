package hu.supercharge.superchargeapp.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import rx.subjects.BehaviorSubject;

public class NetworkService extends BroadcastReceiver {

    protected Context appContext;

    protected ConnectivityManager conMgr;

    private BehaviorSubject<Boolean> isConnected = BehaviorSubject.create(false);

    public NetworkService(Context context) {
        this.appContext = context;
        conMgr = (ConnectivityManager) appContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        checkNetworkState();
        registerReceiver(appContext);
    }

    private void registerReceiver(Context appContext) {
        final IntentFilter mIFNetwork = new IntentFilter();
        mIFNetwork.addAction(ConnectivityManager.CONNECTIVITY_ACTION);
        appContext.registerReceiver(this, mIFNetwork);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        checkNetworkState();
    }

    private void checkNetworkState() {
        NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();
        boolean status = activeNetwork != null && activeNetwork.isConnectedOrConnecting();
        isConnected.onNext(status);
    }

    public BehaviorSubject<Boolean> isConnected() {
        return isConnected;
    }

}