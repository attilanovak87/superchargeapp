package hu.supercharge.superchargeapp.dagger.module;

import android.content.Context;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.supercharge.superchargeapp.service.NetworkService;

@Module
public class ServiceModule {

    @Provides
    @Singleton
    public NetworkService getNetworkService(Context appContext) {
        return new NetworkService(appContext);
    }

}
