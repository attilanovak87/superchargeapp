package hu.supercharge.superchargeapp.dagger.module;

import android.content.Context;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import hu.supercharge.superchargeapp.BuildConfig;
import hu.supercharge.superchargeapp.rest.RestApi;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.CallAdapter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;
import rx.schedulers.Schedulers;

import static com.fasterxml.jackson.databind.DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES;

@Module
public class RestModule {

    @Provides
    @Singleton
    public RestApi getRestApi(CallAdapter.Factory adapter, Cache cache) {
        ObjectMapper jacksonMapper = new ObjectMapper().configure(FAIL_ON_UNKNOWN_PROPERTIES, false);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .client(getClient(BuildConfig.API_TOKEN, cache))
                .addConverterFactory(JacksonConverterFactory.create(jacksonMapper))
                .addCallAdapterFactory(adapter)
                .build();
        return retrofit.create(RestApi.class);
    }

    @Provides
    @Singleton
    public CallAdapter.Factory getAdapter() {
        return RxJavaCallAdapterFactory.createWithScheduler(Schedulers.io());
    }

    @Provides
    @Singleton
    public Cache provideCache(Context appContext) {
        File cacheFile = new File(appContext.getCacheDir(), "http");
        if (!cacheFile.exists()) {
            boolean createdDirs = cacheFile.mkdirs();
        }
        return new Cache(cacheFile, 15 * 1024 * 1024);
    }

    private OkHttpClient getClient(String token, Cache cache) {
        OkHttpClient.Builder okHttpClientBuilder = new OkHttpClient.Builder();
        if (BuildConfig.DEBUG) {
            HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
            interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
            okHttpClientBuilder.addInterceptor(interceptor);
        }
        okHttpClientBuilder.cache(cache);
        if (token != null) {
            okHttpClientBuilder.addInterceptor(chain -> {
                HttpUrl url = chain.request().url()
                        .newBuilder()
                        .addQueryParameter("api_key", token)
                        .addQueryParameter("language", "en-US")
                        .build();
                Request request = chain.request().newBuilder().url(url).build();
                return chain.proceed(request);
            });
        }
        return okHttpClientBuilder.build();
    }

}
