package hu.supercharge.superchargeapp.dagger.component;

import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Component;
import hu.supercharge.superchargeapp.dagger.module.AppModule;
import hu.supercharge.superchargeapp.dagger.module.RestModule;
import hu.supercharge.superchargeapp.dagger.module.ServiceModule;
import hu.supercharge.superchargeapp.rest.RestApi;
import hu.supercharge.superchargeapp.service.NetworkService;

@Singleton
@Component(modules = {AppModule.class, RestModule.class, ServiceModule.class})
public interface AppComponent {

    Context getContext();
    SharedPreferences sharedPreferences();
    RestApi restApi();
    NetworkService networkService();

}
