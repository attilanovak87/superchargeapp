package hu.supercharge.superchargeapp.dagger.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class AppModule {

    private Application appDelegate;

    public AppModule(Application appDelegate) {
        this.appDelegate = appDelegate;
    }

    @Provides
    @Singleton
    Context getAppContext() {
        return appDelegate.getApplicationContext();
    }

    @Provides
    @Singleton
    SharedPreferences getSharedPreferences(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

}
