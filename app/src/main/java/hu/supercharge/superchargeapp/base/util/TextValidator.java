package hu.supercharge.superchargeapp.base.util;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;

import java.util.Map;

public class TextValidator implements TextWatcher {

    public static final String TAG = "TextValidator";

    public enum Rules {

        NONEMPTY(0),
        EMAIL(0),
        PASSWORD_LENGTH(4,12);

        private int[] flags;

        Rules(int... flags) {
            this.flags = flags;
        }

        public int[] getFlags() {
            return flags;
        }
    }

    private Rules rules[];

    protected EditText tx;

    public TextValidator(EditText tx, Rules... rules) {
        this.rules = rules;
        this.tx = tx;
    }

    protected boolean checkEmpty(EditText tx, String errorString) {
        boolean isValid = true;
        if(tx.getText().toString().equals("")) {
            isValid = false;
            setError(tx, errorString);
        }
        else {
            tx.setError(null);
        }
        return isValid;
    }

    protected boolean checkLength(EditText tx, int min, int max, String errorString) {
        boolean isValid = true;
        String st = tx.getText().toString();
        if(!st.equals("" ) && st.length() < min || st.length() > max) {
            isValid = false;
            setError(tx, errorString);
        }
        else {
            tx.setError(null);
        }
        return isValid;
    }

    protected boolean checkEmail(EditText tx, String errorString) {
        String val = tx.getText().toString();
        if(!TextUtils.isEmpty(val) && android.util.Patterns.EMAIL_ADDRESS.matcher(val).matches()) {
            tx.setError(null);
            return true;
        }
        setError(tx, errorString);
        return false;
    }

    private void setError(EditText tx, String err) {
        Log.d(TAG, "Setting Error: "+err);
        tx.setError(err);
    }

    public boolean isValid(EditText tx, String text, Map<Rules, String> messages) {
        if(rules == null || rules.length == 0) return true;
        boolean isValid = true;
        for(int i = 0; i < rules.length; i++) {
            Rules currentRule = rules[i];
            String currentMessage = (messages != null && messages.containsKey(currentRule)) ? messages.get(currentRule) : "";
            if(isValid) {
                isValid = validateRule(tx, currentRule, currentMessage);
            }
        }
        return isValid;
    }

    private boolean validateRule(EditText tx, Rules rule, String message) {
        switch (rule) {
            case NONEMPTY:
                return checkEmpty(tx, message);
            case PASSWORD_LENGTH:
                return checkLength(tx, rule.getFlags()[0], rule.getFlags()[1], message);
            case EMAIL:
                return checkEmail(tx, message);
        }
        return true;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {}

    @Override
    public void afterTextChanged(Editable s) {}

    public boolean validate(Map<Rules, String> messages) {
        String text = tx.getText().toString();
        return isValid(tx, text, messages);
    }

}
