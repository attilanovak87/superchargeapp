package hu.supercharge.superchargeapp.base.model;

import android.databinding.ViewDataBinding;

public abstract class ViewTypeItem {

    private int viewType;

    private int viewResourceId;

    public ViewTypeItem(int viewType, int viewResourceId) {
        this.viewType = viewType;
        this.viewResourceId = viewResourceId;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public int getViewResourceId() {
        return viewResourceId;
    }

    public void setViewResourceId(int viewResourceId) {
        this.viewResourceId = viewResourceId;
    }

    public abstract Object createViewHolder(ViewDataBinding binding, int viewType);

    public void onBind(ViewDataBinding binding) {

    }
}
