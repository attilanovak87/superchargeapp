package hu.supercharge.superchargeapp.base;

import android.content.Context;
import android.content.Intent;
import android.databinding.BaseObservable;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.CallSuper;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

public abstract class BaseViewModel extends BaseObservable {

    public static final String TAG = "BaseViewModel";

    protected Context context;

    protected ViewDataBinding mBinding;

    protected CompositeSubscription subscriptions = new CompositeSubscription();

    protected HashMap<String, Subscription> namedSubscriptions = new LinkedHashMap<>();

    public void onCreate(@Nullable Bundle savedInstanceState) {}

    public void onStart() {}

    public void onPause() {}

    protected void onActivityCreated(@NotNull Bundle savedInstanceState) {}

    public void onDetach() {}

    public void onResume() {}

    @CallSuper
    public void onStop() {
        subscriptions.clear();
        if(!namedSubscriptions.isEmpty()) {
            for(String i : namedSubscriptions.keySet()) {
                Subscription sb = namedSubscriptions.get(i);
                if(!sb.isUnsubscribed()) {
                    sb.unsubscribe();
                }
            }
            namedSubscriptions = new LinkedHashMap<>();
        }
    }

    public void onDestroy() {}

    public void onDestroyView() {}

    public void onActivityResult(int requestCode, int resultCode, Intent data) {}

    protected void subscribe(Subscription sb) {
        subscriptions.add(sb);
    }

    protected void subscribe(String tag, Subscription sb) {
        if(namedSubscriptions.containsKey(tag) && !namedSubscriptions.get(tag).isUnsubscribed()) {
            namedSubscriptions.get(tag).unsubscribe();
        }
        namedSubscriptions.put(tag, sb);
    }

    @NotNull
    public ViewDataBinding getmBinding() {
        return mBinding;
    }

    public void setmBinding(ViewDataBinding mBinding) {
        this.mBinding = mBinding;
    }

    @NotNull
    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    protected <T> Observable.Transformer<T, T> applySchedulers() {
        return observable -> observable.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread());
    }

    protected <T> Observable.Transformer<T, T> applyBackgroundSchedulers() {
        return observable -> observable.subscribeOn(Schedulers.io()).observeOn(Schedulers.io());
    }

    protected <T> Observable.Transformer<T, T> applyRetry() {
        return observable -> observable.retryWhen(errors ->
                errors
                        .zipWith(Observable.range(1, 2), (n, i) -> i)
                        .flatMap(retryCount -> Observable.timer((long) Math.pow(3, retryCount), TimeUnit.SECONDS)));
    }

    protected void onSaveInstanceState(Bundle state) {}

    protected void onNewIntent(Intent i) {}

}
