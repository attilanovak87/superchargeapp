package hu.supercharge.superchargeapp.base.adapter;

import android.support.v7.util.DiffUtil;

import java.util.List;

public class BasicDiffUtil<T> extends DiffUtil.Callback {

    private List<T> oldItems;

    private List<T> newItems;

    public BasicDiffUtil(List<T> oldItems, List<T> newItems) {
        this.oldItems = oldItems;
        this.newItems = newItems;
    }

    @Override
    public int getOldListSize() {
        return 0;
    }

    @Override
    public int getNewListSize() {
        return 0;
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition) {
        return false;
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
        return false;
    }
}
