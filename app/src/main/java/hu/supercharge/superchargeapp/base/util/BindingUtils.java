package hu.supercharge.superchargeapp.base.util;

import android.databinding.BindingAdapter;
import android.support.v4.util.Pair;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import hu.supercharge.superchargeapp.R;
import hu.supercharge.superchargeapp.base.model.BindableString;

public class BindingUtils {

    @BindingAdapter(value = {"app:tag"})
    public static void setTag(final View view, String tag) {
        view.setTag(tag);
    }

    @BindingAdapter(value = {"app:adapter"})
    public static void setAdapter(final RecyclerView recyclerView, RecyclerView.Adapter adapter) {
        recyclerView.setAdapter(adapter);
    }

    @BindingAdapter(value = {"app:adapter"})
    public static void setAdapter(final ViewPager viewPager, PagerAdapter adapter) {
        viewPager.setAdapter(adapter);
    }

    @BindingAdapter(value = {"app:adapter"})
    public static void setAdapter(final Spinner spinner, SpinnerAdapter adapter) {
        spinner.setAdapter(adapter);
    }

    @BindingAdapter(value = {"app:listener"})
    public static void setListener(final Spinner spinner, AdapterView.OnItemSelectedListener listener) {
        spinner.setOnItemSelectedListener(listener);
    }

    @BindingAdapter(value = {"app:page"})
    public static void setCurrentPage(final ViewPager viewPager, Integer page) {
        if(page != null) {
            viewPager.setCurrentItem(page);
        }
    }

    @BindingAdapter({"app:binding"})
    public static void bindEditText(EditText view, final BindableString bindableString) {
        Pair<BindableString, TextWatcherAdapter> pair = (Pair) view.getTag(R.id.bound_observable);
        if (pair == null || pair.first != bindableString) {
            if (pair != null) {
                view.removeTextChangedListener(pair.second);
            }
            TextWatcherAdapter watcher = new TextWatcherAdapter() {
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    bindableString.set(s.toString());
                }
            };
            view.setTag(R.id.bound_observable, new Pair<>(bindableString, watcher));
            view.addTextChangedListener(watcher);
        }
        String newValue = bindableString.get();
        if (!view.getText().toString().equals(newValue)) {
            view.setText(newValue);
        }
    }

    @BindingAdapter({"app:validator"})
    public static void addValidator(final EditText view, TextValidator.Rules... rules) {
        TextValidator validator = new TextValidator(view, rules);
        view.setTag(R.id.validator, validator);
    }

    @BindingAdapter({"app:validator"})
    public static void addValidatorS(final EditText view, TextValidator.Rules rules) {
        TextValidator validator = new TextValidator(view, rules);
        view.setTag(R.id.validator, validator);
    }


}
