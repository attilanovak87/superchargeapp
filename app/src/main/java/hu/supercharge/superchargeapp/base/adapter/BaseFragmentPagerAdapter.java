package hu.supercharge.superchargeapp.base.adapter;

import android.databinding.ObservableInt;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import hu.supercharge.superchargeapp.base.util.SizedStack;

public abstract class BaseFragmentPagerAdapter extends FragmentStatePagerAdapter {

    public static final String PARAM_SWIPE = "paramSwipe";
    public static final String SWIPE_LEFT = "swipeLeft";
    public static final String SWIPE_RIGHT = "swipeRight";

    private ObservableInt currentPage = new ObservableInt(0);

    private SizedStack<Integer> visitedPages = new SizedStack<>(10);

    public BaseFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public void nextPage() {
        setCurrentPage(getNextPage());
    }

    public void previousPage() {
        setCurrentPage(getPreviousPage());
    }

    public boolean canGoBack() {
        return (visitedPages.size() > 1);
    }

    public boolean goBack() {
        if(canGoBack()) {
            visitedPages.pop();
            Integer page = visitedPages.pop();
            setCurrentPage(page);
            return true;
        }
        return false;
    }

    public int getNextPage() {
        return (currentPage.get() + 1 > (getCount()-1)) ? currentPage.get() : currentPage.get() + 1;
    }

    public int getPreviousPage() {
        return (currentPage.get() - 1 < 0) ? 0 : currentPage.get() - 1;
    }

    public ObservableInt getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int page) {
        currentPage.set(page);
        visitedPages.push(page);
    }

}
