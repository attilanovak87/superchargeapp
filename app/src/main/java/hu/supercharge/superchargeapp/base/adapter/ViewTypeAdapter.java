package hu.supercharge.superchargeapp.base.adapter;

import android.databinding.ViewDataBinding;
import android.view.ViewGroup;

import hu.supercharge.superchargeapp.base.model.ViewTypeItem;
import hu.supercharge.superchargeapp.base.rv.BindableViewHolder;

public abstract class ViewTypeAdapter<T extends BindableViewHolder<ViewDataBinding, M, ViewTypeItem>, M> extends BaseAdapter<ViewTypeItem, ViewDataBinding, T, M> {

    private M viewModel;

    public ViewTypeAdapter() {}

    public ViewTypeAdapter(M viewModel) {
        this.viewModel = viewModel;
    }

    @Override
    public int getItemViewType(int position) {
        if(getItems().isEmpty()) return super.getItemViewType(position);
        return getItems().get(position).getViewType();
    }

    @Override
    protected int getViewResourceId(ViewGroup parent, int viewType) {
        for(ViewTypeItem i: items) {
            if(i.getViewType() == viewType) {
                return i.getViewResourceId();
            }
        }
        throw new RuntimeException("Resource id cannot be null: "+this.getClass().getCanonicalName());
    }

    @Override
    protected T createViewHolder(ViewDataBinding binding, int viewType) {
        for(ViewTypeItem i: items) {
            if(i.getViewType() == viewType) {
                return (T) i.createViewHolder(binding, viewType);
            }
        }
        throw new RuntimeException("Viewholder cannot be null: "+this.getClass().getCanonicalName());
    }

    @Override
    protected M createViewModel(ViewDataBinding binding, int viewType) {
        return viewModel;
    }

    public void setViewModel(M viewModel) {
        this.viewModel = viewModel;
    }
}
