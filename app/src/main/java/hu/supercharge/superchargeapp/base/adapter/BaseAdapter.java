package hu.supercharge.superchargeapp.base.adapter;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import hu.supercharge.superchargeapp.base.rv.BindableViewHolder;

public abstract class BaseAdapter<K extends Object, N extends ViewDataBinding, T extends BindableViewHolder<N, M, K>, M> extends RecyclerView.Adapter<T> {

    protected M viewModel;

    protected List<K> items = new ArrayList<>();

    public BaseAdapter() {

    }

    public BaseAdapter(List<K> items) {
        this.items = items;
    }

    @Override
    public T onCreateViewHolder(ViewGroup parent, int viewType) {
        N binding = createBinding(parent, viewType);
        T holder = createViewHolder(binding, viewType);
        holder.setmBinding(binding);
        viewModel = createViewModel(binding, viewType);
        holder.setmBinding(binding);
        holder.setViewModel(viewModel);
        holder.setViewType(viewType);
        holder.onCreated();
        return holder;
    }

    @Override
    public void onBindViewHolder(T holder, int position) {
        holder.onBind(items.get(position));
    }

    @Override
    public int getItemCount() {
        return items != null ? items.size() : 0;
    }

    public synchronized void add(int index, K item) {
        items.add(index, item);
        notifyItemInserted(index);
    }

    public synchronized void replace(int index, K item) {
        items.set(index, item);
        notifyItemChanged(index);
    }

    public synchronized void add(K item) {
        items.add(item);
        notifyItemInserted(items.size());
    }

    public synchronized void addAll(List<K> itemList, boolean clear) {
        if(clear) clear();
        addAll(itemList);
    }

    public synchronized void addAll(List<K> itemList) {
        int beforeSize = items.size();
        items.addAll(itemList);
        notifyItemRangeInserted(beforeSize, items.size());
    }

    public synchronized void clear() {
        int beforeSize = items.size();
        items.clear();
        notifyItemRangeRemoved(0, beforeSize);
    }

    public synchronized void delete(int index) {
        items.remove(index);
        notifyItemRemoved(index);
    }

    public synchronized void delete(K item) {
        int itemIndex = items.indexOf(item);
        items.remove(item);
        notifyItemRemoved(itemIndex);
    }

    public List<K> getItems() {
        return items;
    }

    public void setItems(List<K> items) {
        this.items = items;
    }

    protected N createBinding(ViewGroup parent, int viewType) {
        return (N) DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), getViewResourceId(parent, viewType), parent, false);
    }

    protected abstract int getViewResourceId(ViewGroup parent, int viewType);

    protected abstract T createViewHolder(N binding, int viewType);

    protected abstract M createViewModel(N binding, int viewType);

    @Override
    public void onViewAttachedToWindow(T holder) {
        super.onViewAttachedToWindow(holder);
        holder.onAttachedToWindow();
    }

    @Override
    public void onViewDetachedFromWindow(T holder) {
        super.onViewDetachedFromWindow(holder);
        holder.onDetachedFromWindow();
    }
}
