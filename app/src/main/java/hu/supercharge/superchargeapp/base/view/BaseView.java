package hu.supercharge.superchargeapp.base.view;

import android.databinding.ViewDataBinding;

import hu.supercharge.superchargeapp.base.BaseViewModel;

public interface BaseView<T extends ViewDataBinding> {

    T getBinding();

    BaseViewModel getViewModel();
}
