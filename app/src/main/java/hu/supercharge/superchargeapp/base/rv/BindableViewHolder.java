package hu.supercharge.superchargeapp.base.rv;

import android.databinding.ViewDataBinding;
import android.support.v7.widget.RecyclerView;
import android.view.View;

public abstract class BindableViewHolder<K extends ViewDataBinding, T, M> extends RecyclerView.ViewHolder {

    protected int viewType;

    protected K mBinding;

    private T viewModel;

    public BindableViewHolder(View itemView) {
        super(itemView);
    }

    public void onCreated() {
    }

    public void onAttachedToWindow() {
    }

    public void onDetachedFromWindow() {
    }

    public abstract void onBind(M item);

    public K getBinding() {
        return mBinding;
    }

    public void setmBinding(K mBinding) {
        this.mBinding = mBinding;
    }

    public int getViewType() {
        return viewType;
    }

    public void setViewType(int viewType) {
        this.viewType = viewType;
    }

    public T getViewModel() {
        return viewModel;
    }

    public void setViewModel(T viewModel) {
        this.viewModel = viewModel;
    }
}
