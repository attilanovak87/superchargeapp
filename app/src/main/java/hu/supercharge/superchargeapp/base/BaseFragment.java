package hu.supercharge.superchargeapp.base;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import javax.inject.Inject;

public abstract class BaseFragment<T extends BaseViewModel> extends Fragment {

    public static final String TAG = "BaseFragment";

    @Inject
    protected T viewModel;

    protected ViewDataBinding viewBinding;

    protected T getViewModel() {
        return viewModel;
    }

    protected abstract void inject(Context context);

    protected abstract void cleanup();

    @Override
    @CallSuper
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel.onActivityCreated(savedInstanceState);
    }

    @Override
    @CallSuper
    public void onDetach() {
        if (viewModel != null) {
            viewModel.onDetach();
        }
        if (viewBinding != null) {
            viewBinding.unbind();
            viewBinding = null;
        }
        viewModel = null;
        super.onDetach();
    }

    @Override
    @CallSuper
    public void onStart() {
        super.onStart();
        viewModel.onStart();
    }

    @Override
    @CallSuper
    public void onStop() {
        viewModel.onStop();
        super.onStop();
    }

    @Override
    @CallSuper
    public void onPause() {
        viewModel.onPause();
        super.onPause();
    }

    @Override
    @CallSuper
    public void onResume() {
        super.onResume();
        viewModel.onResume();
    }

    @Override
    @CallSuper
    public void onDestroy() {
        viewModel.onDestroy();
        viewModel = null;
        viewBinding = null;
        super.onDestroy();
    }

    @Nullable
    @Override
    @CallSuper
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        inject(getContext());
        viewBinding = createBinding(inflater, container, savedInstanceState);
        viewModel.setContext(getContext());
        viewModel.setmBinding(viewBinding);
        onBindingCreated(viewBinding);
        return viewBinding.getRoot();
    }

    @Override
    @CallSuper
    public void onDestroyView() {
        viewModel.onDestroyView();
        cleanup();
        super.onDestroyView();
    }

    protected ViewDataBinding createBinding(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return DataBindingUtil.inflate(inflater, getViewResourceId(), container, false);
    }

    protected abstract int getViewResourceId();

    protected void onBindingCreated(ViewDataBinding viewBinding) {
        //viewBinding.setVariable(BR.viewModel, getViewModel());
    }

    @Override
    @CallSuper
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        viewModel.onCreate(savedInstanceState);
    }

    @Override
    @CallSuper
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(viewModel != null) {
            viewModel.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        viewModel.onActivityResult(requestCode, resultCode, data);
    }

    public void onNewIntent(Intent intent) {
        viewModel.onNewIntent(intent);
    }
}
