package hu.supercharge.superchargeapp.base.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;

public abstract class DatabindingDialog extends Dialog {

    private int viewResId;

    private ViewDataBinding binding;

    public DatabindingDialog(@NonNull Context context, int viewResId) {
        super(context);
        initView();
    }

    public DatabindingDialog(@NonNull Context context, int viewResId, int themeResId) {
        super(context, themeResId);
        this.viewResId = viewResId;
        initView();
    }

    protected void initView() {
        binding = DataBindingUtil.inflate(getLayoutInflater(), viewResId, null, false);
        addBinding(binding);
        binding.executePendingBindings();
        setContentView(binding.getRoot());
    }

    protected abstract void addBinding(ViewDataBinding binding);

    public int getViewResId() {
        return viewResId;
    }

    public void setViewResId(int viewResId) {
        this.viewResId = viewResId;
    }

    public ViewDataBinding getBinding() {
        return binding;
    }

    public void setBinding(ViewDataBinding binding) {
        this.binding = binding;
    }
}
