package hu.supercharge.superchargeapp.base.ui.dialog;

import android.content.Context;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;

import hu.supercharge.superchargeapp.base.view.BaseView;

public abstract class ViewModelDialog<T extends BaseView> extends DatabindingDialog {

    private T viewModel;

    public ViewModelDialog(@NonNull Context context, T viewModel, int viewResId, int themeResId) {
        super(context, viewResId, themeResId);
        this.viewModel = viewModel;
        addBinding(getBinding());
    }

    @Override
    protected void addBinding(ViewDataBinding binding) {
        bindViewModel(getViewModel());
        binding.executePendingBindings();
    }

    public T getViewModel() {
        return viewModel;
    }

    protected abstract void bindViewModel(T model);
}
