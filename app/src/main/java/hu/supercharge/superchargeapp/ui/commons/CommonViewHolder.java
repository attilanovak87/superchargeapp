package hu.supercharge.superchargeapp.ui.commons;

import android.databinding.ViewDataBinding;
import android.view.View;

import hu.supercharge.superchargeapp.base.model.ViewTypeItem;
import hu.supercharge.superchargeapp.base.rv.BindableViewHolder;

public class CommonViewHolder<T> extends BindableViewHolder<ViewDataBinding, T, ViewTypeItem> {

    public CommonViewHolder(View itemView) {
        super(itemView);
    }

    @Override
    public void onBind(ViewTypeItem item) {
        item.onBind(getBinding());
    }

}
