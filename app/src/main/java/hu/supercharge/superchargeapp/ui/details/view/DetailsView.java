package hu.supercharge.superchargeapp.ui.details.view;

import android.databinding.ViewDataBinding;

import hu.supercharge.superchargeapp.base.view.BaseView;

public interface DetailsView extends BaseView<ViewDataBinding> {

    default ViewDataBinding getBinding() {
        return (ViewDataBinding) getViewModel().getmBinding();
    }

}