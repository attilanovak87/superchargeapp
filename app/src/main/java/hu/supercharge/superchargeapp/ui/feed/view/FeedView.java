package hu.supercharge.superchargeapp.ui.feed.view;

import android.databinding.ObservableField;
import android.support.v7.widget.RecyclerView;
import android.text.TextWatcher;

import hu.supercharge.superchargeapp.base.view.BaseView;
import hu.supercharge.superchargeapp.databinding.FragmentFeedBinding;
import hu.supercharge.superchargeapp.model.Movie;

public interface FeedView extends BaseView<FragmentFeedBinding> {

    void onShowDetails(Movie item);

    default void setHasResults(ObservableField<Boolean> hasResults) {
        getBinding().setHasResults(hasResults);
    }

    default void setSearchText(ObservableField<String> text, TextWatcher watcher) {
        getBinding().setSearchText(text);
        getBinding().searchText.addTextChangedListener(watcher);
    }

    default void setAdapter(RecyclerView.Adapter adapter) {
        getBinding().setAdapter(adapter);
    }

    default FragmentFeedBinding getBinding() {
        return (FragmentFeedBinding) getViewModel().getmBinding();
    }

}