package hu.supercharge.superchargeapp.ui.details;

import javax.inject.Inject;

import hu.supercharge.superchargeapp.base.BaseViewModel;
import hu.supercharge.superchargeapp.ui.commons.CommonViewModel;
import hu.supercharge.superchargeapp.ui.details.view.DetailsView;

public class DetailsViewModel extends CommonViewModel implements DetailsView {

    @Inject
    public DetailsViewModel() {}

    public void loadMovie(int movieId) {

    }

    @Override
    public BaseViewModel getViewModel() {
        return this;
    }

}