package hu.supercharge.superchargeapp.ui.details;

import android.content.Context;
import android.os.Bundle;

import hu.supercharge.superchargeapp.R;
import hu.supercharge.superchargeapp.ui.commons.CommonsFragment;
import hu.supercharge.superchargeapp.ui.details.component.DaggerDetailsComponent;
import hu.supercharge.superchargeapp.ui.details.component.DetailsComponent;

public class DetailsFragment extends CommonsFragment<DetailsViewModel, DetailsComponent> {

    private static final String PARAM_MOVIE_ID = "movieId";

    private DetailsComponent component;

    public static DetailsFragment getInstance(Integer movieId) {
        Bundle args = new Bundle();
        args.putInt(PARAM_MOVIE_ID, movieId);
        DetailsFragment fragment= new DetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected void inject(Context context) {
        component = DaggerDetailsComponent.builder().appComponent(getAppComponent()).build();
        component.inject(this);
        component.inject(getViewModel());
        Bundle args = getArguments();
        if(args.getInt(PARAM_MOVIE_ID, -1) != -1) {
            int movieId = args.getInt(PARAM_MOVIE_ID);
            getViewModel().loadMovie(movieId);
        }
    }

    @Override
    protected void cleanup() {
        component = null;
    }

    @Override
    protected int getViewResourceId() {
        return R.layout.fragment_details;
    }

}