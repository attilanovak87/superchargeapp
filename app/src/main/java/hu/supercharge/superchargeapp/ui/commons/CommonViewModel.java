package hu.supercharge.superchargeapp.ui.commons;

import hu.supercharge.superchargeapp.base.BaseViewModel;
import rx.Observable;

public class CommonViewModel extends BaseViewModel{

    protected String s(int id) {
        return getContext().getString(id);
    }

    protected void toast(String message) {

    }

    protected <T> Observable.Transformer<T, T> applyBasicRestTransformers() {
        return observable -> observable
                .compose(applySchedulers())
                .compose(applyRetry());
    }

}
