package hu.supercharge.superchargeapp.ui.feed.adapter;

import android.view.ViewGroup;

import javax.inject.Inject;

import hu.supercharge.superchargeapp.R;
import hu.supercharge.superchargeapp.base.adapter.BaseAdapter;
import hu.supercharge.superchargeapp.dagger.scope.FragmentScope;
import hu.supercharge.superchargeapp.databinding.ViewFeedMovieItemBinding;
import hu.supercharge.superchargeapp.model.Movie;
import hu.supercharge.superchargeapp.ui.feed.FeedViewModel;
import hu.supercharge.superchargeapp.ui.feed.view.FeedView;
import hu.supercharge.superchargeapp.ui.feed.viewholder.FeedBaseViewHolder;

@FragmentScope
public class FeedAdapter extends BaseAdapter<Movie, ViewFeedMovieItemBinding, FeedBaseViewHolder, FeedView> {

    private FeedViewModel viewModel;

    @Inject
    public FeedAdapter() {}

    @Override
    protected int getViewResourceId(ViewGroup parent, int viewType) {
        return R.layout.view_feed_movie_item;
    }

    @Override
    protected FeedBaseViewHolder createViewHolder(ViewFeedMovieItemBinding binding, int viewType) {
        return new FeedBaseViewHolder(binding.getRoot(), viewModel.getGlideManager(), viewModel.getAppContext());
    }

    @Override
    protected FeedView createViewModel(ViewFeedMovieItemBinding binding, int viewType) {
        return viewModel;
    }

    public void setViewModel(FeedViewModel viewModel) {
        this.viewModel = viewModel;
    }

}
