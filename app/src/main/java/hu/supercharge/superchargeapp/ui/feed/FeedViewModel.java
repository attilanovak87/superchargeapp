package hu.supercharge.superchargeapp.ui.feed;

import android.content.Context;
import android.databinding.ObservableField;
import android.os.Bundle;
import android.support.annotation.MainThread;
import android.text.Editable;
import android.text.TextWatcher;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import org.jetbrains.annotations.NotNull;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Objects;

import javax.inject.Inject;

import hu.supercharge.superchargeapp.BuildConfig;
import hu.supercharge.superchargeapp.base.BaseViewModel;
import hu.supercharge.superchargeapp.base.util.TextWatcherAdapter;
import hu.supercharge.superchargeapp.model.Movie;
import hu.supercharge.superchargeapp.model.SearchResponse;
import hu.supercharge.superchargeapp.rest.RestApi;
import hu.supercharge.superchargeapp.ui.commons.CommonViewModel;
import hu.supercharge.superchargeapp.ui.feed.adapter.FeedAdapter;
import hu.supercharge.superchargeapp.ui.feed.view.FeedView;
import rx.Observable;

public class FeedViewModel extends CommonViewModel implements FeedView {

    @Inject
    protected Context appContext;

    private RestApi restApi;
    private FeedAdapter adapter;

    private Locale currencyLocale = new Locale("en", "US");
    private NumberFormat formatter = NumberFormat.getCurrencyInstance(currencyLocale);

    private ObservableField<String> searchText = new ObservableField<>();

    private TextWatcher searchWatcher = new TextWatcherAdapter() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {}

        @Override
        public void afterTextChanged(Editable s) {
            if (!Objects.equals(searchText.get(), s.toString())) {
                searchText.set(s.toString());
            }
        }
    };

    private ObservableField<Boolean> hasResults = new ObservableField<>(false);

    @Inject
    public FeedViewModel(RestApi restApi, FeedAdapter adapter) {
        this.restApi = restApi;
        this.adapter = adapter;
    }

    @Override
    protected void onActivityCreated(@NotNull Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initRecycleView();
        initSearch();
    }

    private void initSearch() {
        setHasResults(hasResults);
        setSearchText(searchText, searchWatcher);
        searchText.addOnPropertyChangedCallback(new OnPropertyChangedCallback() {
            @Override
            public void onPropertyChanged(android.databinding.Observable sender, int propertyId) {
                loadFeed(searchText.get());
            }
        });
    }

    private void initRecycleView() {
        adapter.setViewModel(this);
        setAdapter(adapter);
    }

    private void loadFeed(String searchTerm) {
        if(searchTerm == null || searchTerm.length() < 2) return;
        adapter.clear();
        subscribe("feed",
            restApi
                .getMovies(searchTerm, 1, false)
                .map(SearchResponse::getMovies)
                .flatMap(Observable::from)
                .doOnCompleted(this::onFeedLoadCompleted)
                .compose(applyBasicRestTransformers())
                .subscribe(this::onFeedLoaded, this::onFeedLoadError)
        );
    }

    @MainThread
    private void onFeedLoaded(Movie item) {
        item.setPosterBaseUrl(BuildConfig.IMAGE_BASE_URL);
        adapter.add(item);
        subscribe(
            restApi.getMovieDetails(item.getId()).compose(applyBasicRestTransformers()).subscribe(res -> {
                if(res.getBudget() == null || res.getBudget() == 0) {
                    item.setBudget("-");
                }
                else {
                    item.setBudget(formatter.format(res.getBudget()));
                }
            })
        );
    }

    private void onFeedLoadCompleted() {
        hasResults.set(adapter.getItemCount() != 0);
    }

    private void onFeedLoadError(Throwable th) {
        th.printStackTrace();
    }

    @Override
    public void onShowDetails(Movie item) {

    }

    public RequestManager getGlideManager() {
        return Glide.with(getContext());
    }

    public Context getAppContext() {
        return appContext;
    }

    @Override
    public BaseViewModel getViewModel() {
        return this;
    }

}