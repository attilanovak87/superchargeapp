package hu.supercharge.superchargeapp.ui.feed.dagger;

import dagger.Component;
import hu.supercharge.superchargeapp.dagger.component.AppComponent;
import hu.supercharge.superchargeapp.dagger.scope.FragmentScope;
import hu.supercharge.superchargeapp.ui.feed.FeedFragment;
import hu.supercharge.superchargeapp.ui.feed.FeedViewModel;

@FragmentScope
@Component(dependencies = AppComponent.class)
public interface FeedComponent {

    FeedFragment inject(FeedFragment fragment);

    FeedViewModel inject(FeedViewModel viewModel);

}
