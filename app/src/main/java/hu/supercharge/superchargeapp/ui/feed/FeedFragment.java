package hu.supercharge.superchargeapp.ui.feed;

import android.content.Context;

import javax.inject.Inject;

import hu.supercharge.superchargeapp.R;
import hu.supercharge.superchargeapp.ui.commons.CommonsFragment;
import hu.supercharge.superchargeapp.ui.feed.dagger.DaggerFeedComponent;
import hu.supercharge.superchargeapp.ui.feed.dagger.FeedComponent;


public class FeedFragment extends CommonsFragment<FeedViewModel, FeedComponent> {

    private FeedComponent component;

    @Inject
    public FeedFragment() {
        setRetainInstance(true);
    }

    @Override
    protected void inject(Context context) {
        component = DaggerFeedComponent.builder().appComponent(getAppComponent()).build();
        component.inject(this);
        component.inject(getViewModel());
    }

    @Override
    protected void cleanup() {
        component = null;
    }

    @Override
    protected int getViewResourceId() {
        return R.layout.fragment_feed;
    }

}