package hu.supercharge.superchargeapp.ui.commons;

import hu.supercharge.superchargeapp.AppDelegate;
import hu.supercharge.superchargeapp.base.BaseFragment;
import hu.supercharge.superchargeapp.base.BaseViewModel;
import hu.supercharge.superchargeapp.dagger.component.AppComponent;

public abstract class CommonsFragment<T extends BaseViewModel, K> extends BaseFragment<T> {

    protected K component;

    @Override
    protected void cleanup() {
        component = null;
    }

    protected AppComponent getAppComponent() {
        return AppDelegate.getInstance().getAppComponent();
    }

}
