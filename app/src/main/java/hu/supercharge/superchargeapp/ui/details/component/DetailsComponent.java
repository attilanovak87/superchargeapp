package hu.supercharge.superchargeapp.ui.details.component;

import dagger.Component;
import hu.supercharge.superchargeapp.dagger.component.AppComponent;
import hu.supercharge.superchargeapp.dagger.scope.FragmentScope;
import hu.supercharge.superchargeapp.ui.details.DetailsFragment;
import hu.supercharge.superchargeapp.ui.details.DetailsViewModel;

@FragmentScope
@Component(dependencies = AppComponent.class)
public interface DetailsComponent {

    DetailsFragment inject(DetailsFragment fragment);

    DetailsViewModel inject(DetailsViewModel viewModel);

}
