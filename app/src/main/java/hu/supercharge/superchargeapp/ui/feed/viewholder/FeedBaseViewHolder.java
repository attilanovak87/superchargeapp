package hu.supercharge.superchargeapp.ui.feed.viewholder;

import android.content.Context;
import android.view.View;

import com.bumptech.glide.RequestManager;

import hu.supercharge.superchargeapp.R;
import hu.supercharge.superchargeapp.base.rv.BindableViewHolder;
import hu.supercharge.superchargeapp.databinding.ViewFeedMovieItemBinding;
import hu.supercharge.superchargeapp.model.Movie;
import hu.supercharge.superchargeapp.ui.feed.view.FeedView;

public class FeedBaseViewHolder extends BindableViewHolder<ViewFeedMovieItemBinding, FeedView, Movie> {

    private Context context;

    private RequestManager glideManager;

    public FeedBaseViewHolder(View itemView, RequestManager glideManager, Context context) {
        super(itemView);
        this.glideManager = glideManager;
        this.context = context;
    }

    @Override
    public void onBind(Movie item) {
        getBinding().setViewModel(getViewModel());
        getBinding().setItem(item);
        String posterPath = item.getPosterPath();
        getBinding().posterImage.setImageDrawable(context.getResources().getDrawable(R.drawable.placeholder));
        if(posterPath != null) {
            String fullPath = item.getPosterFullPath("w185");
            glideManager.load(fullPath).into(getBinding().posterImage);
        }
    }
}
