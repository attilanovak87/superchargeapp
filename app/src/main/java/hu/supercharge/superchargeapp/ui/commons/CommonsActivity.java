package hu.supercharge.superchargeapp.ui.commons;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import hu.supercharge.superchargeapp.R;

public class CommonsActivity extends AppCompatActivity {

    protected void setFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.contentView, fragment, fragment.getClass().getCanonicalName())
                .addToBackStack(fragment.getClass().getCanonicalName())
                .commit();
    }

    protected boolean isFragmentAlive(Fragment fragment) {
        return (fragment.isAdded() && !fragment.isDetached() && !fragment.isRemoving());
    }

}
