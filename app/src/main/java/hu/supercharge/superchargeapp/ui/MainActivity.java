package hu.supercharge.superchargeapp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;

import hu.supercharge.superchargeapp.R;
import hu.supercharge.superchargeapp.ui.commons.CommonsActivity;
import hu.supercharge.superchargeapp.ui.feed.FeedFragment;

public class MainActivity extends CommonsActivity {

    public static final String ACTION_SHOW_DETAILS = "hu.supercharge.superchargeapp.SHOW_DETAILS";
    public static final String PARAM_MOVIE_ID = "movieId";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(savedInstanceState == null) {
            setFragment(new FeedFragment());
        }
    }

}
