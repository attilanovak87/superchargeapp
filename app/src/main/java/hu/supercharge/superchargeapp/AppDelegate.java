package hu.supercharge.superchargeapp;

import android.app.Application;

import hu.supercharge.superchargeapp.dagger.component.AppComponent;
import hu.supercharge.superchargeapp.dagger.component.DaggerAppComponent;
import hu.supercharge.superchargeapp.dagger.module.AppModule;
import hu.supercharge.superchargeapp.dagger.module.RestModule;
import hu.supercharge.superchargeapp.dagger.module.ServiceModule;
import timber.log.Timber;

public class AppDelegate extends Application {

    private AppComponent mAppComponent;

    private static AppDelegate mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        Timber.plant(new Timber.DebugTree());
        initAppComponent();
    }

    private void initAppComponent() {
        mAppComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .restModule(new RestModule())
                .serviceModule(new ServiceModule())
                .build();
    }

    public AppComponent getAppComponent() {
        return mAppComponent;
    }

    public static  AppDelegate getInstance() {
        return mInstance;
    }
}
